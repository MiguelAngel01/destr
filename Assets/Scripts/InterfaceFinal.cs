﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InterfaceFinal : MonoBehaviour
{
    
    public Text textThrown;
    public Text textDestroyed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textThrown.text = "Number of Objects: " + GameManager.currentNumberStonesThrown;
        textDestroyed.text = "Points: " + GameManager.currentNumberDestroyedStones;
    }

    public void Click()
    {
        SceneManager.LoadScene("Awake");
    }
}
