﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceStone : MonoBehaviour
{

    public Text textThrown;
    public Text textDestroyed;
	private AudioSource audio;
    
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
		audio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        textThrown.text = "Number of Objects: " + GameManager.currentNumberStonesThrown;
        textDestroyed.text = "Points: " + GameManager.currentNumberDestroyedStones;
    }
}
