using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour
{

    private const float yDie = -30.0f;

	private int smashes = 0;

    public GameObject explosion;

    private const string ROAD_ROLLER = "Object3";
    private const string CAR = "Object2";
    private const string STONE = "Object1";
    private const string BUILD = "Building";
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < yDie)
        {
            Destroy(gameObject);
		}
    }

    void OnMouseDown()
    {
        if (Input.touchCount > 0)
        {
            
            for (int i = 0; i < Input.touchCount; i++)
            { 
				
               if (gameObject.tag == ROAD_ROLLER){
				   smashes++;
				   if (smashes >= 3){
				        Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 4.00f);
            		    Destroy(gameObject);
            		    GameManager.currentNumberDestroyedStones++;
				    }
			    } else if (gameObject.tag == CAR){
                    smashes++;
                    if (smashes >= 2)
                    {
                        Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 4.00f);
                        Destroy(gameObject);
                        GameManager.currentNumberDestroyedStones++;
                    }
                } else if (gameObject.tag == STONE){
            	    Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 4.00f);
            	    Destroy(gameObject);
            	    GameManager.currentNumberDestroyedStones++;
			    } else if (gameObject.tag == BUILD)
               {
	               Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 4.00f);
	               Destroy(gameObject);
	               GameManager.currentNumberDestroyedStones -= 2;
               }
                
            }
            
        }
        else
        {

			if (gameObject.tag == ROAD_ROLLER)
            {
				smashes++;
				if (smashes >= 3){
					Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 4.00f);
            		Destroy(gameObject);
            		GameManager.currentNumberDestroyedStones++;
				}
			} else if (gameObject.tag == CAR){
                    smashes++;
                    if (smashes >= 2)
                    {
                        Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 4.00f);
                        Destroy(gameObject);
                        GameManager.currentNumberDestroyedStones++;
                    }
            } else if (gameObject.tag == STONE){
            	Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 4.00f);
            	Destroy(gameObject);
            	GameManager.currentNumberDestroyedStones++;
			} else if (gameObject.tag == BUILD)
			{
				Destroy(Instantiate(explosion, transform.position, Quaternion.identity), 4.00f);
				Destroy(gameObject);
				GameManager.currentNumberDestroyedStones -= 2;
			}
        }
        
        
    }
}
